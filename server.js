const express = require("express")
const app = express();

app.use(express.static(__dirname + '/public'))

app.get("/",function(req,res){
    res.sendFile(__dirname + "/index.html")
})

app.listen(4548, function(){
    console.log("Server running on port 4548")
})